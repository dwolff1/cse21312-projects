/**********************************************
* File: AVLAuthorTest.cpp
* Author: Dimitri Wolff
* Email: dwolff1@nd.edu
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct author {
    std::string first_name;
    std::string last_name;
    
    author(  std::string first_name, std::string last_name):first_name(first_name),last_name(last_name) {};
    
    bool operator<(const author& rhs) const{
            if (last_name<rhs.last_name)
                return true;
                else if(last_name == rhs.last_name){
                    if (first_name<rhs.first_name)
                        return true;
            }
            return false;
    }
    
    bool operator==(const author& rhs) const{
        if (last_name!=rhs.last_name)
            return false;
        else {
            if (first_name!=rhs.first_name)
                return false;
        }
        return true;
    }
    //Need a friend function
    friend std::ostream& operator<<(std::ostream& outStream, const author& printAuth){
        return outStream;
    }
    
} ;
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
    AVLTree<author> authorsAVL;
    author One("Anthony","Aardvark");
        //One.first_name("Michael");
        //One.last_name("Wolff");
    author Two("Gregory","Aardvark");
    author Three("Bad","Person");
    author Four("Gayle","McDowell");
    author Five("Michael","Main");
    author Six("Walter","Savitch");
    
    authorsAVL.insert(One);
    authorsAVL.insert(Two);
    authorsAVL.insert(Three);
    authorsAVL.insert(Four);
    authorsAVL.insert(Five);
    authorsAVL.insert(Six);
    authorsAVL.printTree();
    std::cout << "Removing " << Three << std::endl;
    authorsAVL.remove(Three);
    authorsAVL.printTree();
    return 0;
}
