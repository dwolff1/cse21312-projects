 * File: readme.txt
 * Author: Dimitri Wolff
 * Email: dwolff1@nd.edu

This is the program for the database problem (problem #1) of the Weekly Coding Report. The homework1_1.cpp is the main driver and BTree.h and BTreeNode.h are the libraries.
Use the following command to compile:
 	gcc -std=c++11 -o run homework1_1.cpp 
Use the following command to run:
 	./run
