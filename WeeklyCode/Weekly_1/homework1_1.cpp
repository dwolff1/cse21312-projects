/**********************************************
* File: homework1_1.cpp
* Author: Dimitri Wolff
* Email: dwolff1@nd.edu
*
* This is the driver function for your weekly
* assignment
*
* Must push the following files to GitLab
* BTreeInClass.cpp
* BTree.h - modified with your
* BTreeNode.h - modified with your
**********************************************/
#include "BTree.h"
#include <string>
#include <vector>
#include <time.h>
#include <array>

#define KEY_LIMIT 1000
#define TEST_KEY_LIMIT 10


/********************************************
* Function Name  : Data 
* Pre-conditions : int data, int key;
* Post-conditions: 
*  
*  Struct for storing the key value and data value
*  for the database
********************************************/
struct Data {
    int data;
    int key;
    //default constructor
    Data():data(0),key(0){};
    
    // Data constructor
    Data( int data, int key ):data(data),key(key) {};
    
    //Less than operator
    bool operator<(const Data& rhs) const{
        if (key<rhs.key)
            return true;
        return false;
    }
    //More than operator
    bool operator>(const Data& rhs) const{
        if (key>rhs.key)
            return true;
        return false;
    }
    //Equal to operator
    bool operator==(const Data& rhs) const{
        if (key!=rhs.key )
            return false;
        return true;
    }
    friend std::ostream& operator<<(std::ostream& outStream, const Data& printData){
        outStream<<" "<<printData.key<<" with data value "<<printData.data;
        return outStream;
    }
    
} ;
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
* This is the main driver function for the program.
* Using two random number generators and a while loop,
* generates 1000 keys and values, and insert them into
* the database. Then, uses the random number generator
* to test if 10 random key are in the database, and 
* prints the key and data.
********************************************/
int main(int argc, char** argv){
    srand (time(NULL));
	// Initial test code
	BTree<Data> database(2);
    //std::vector <Data> temp_struct;
    int i=0;
    // Random number generator outputting 1000 keys and values
    while(i<KEY_LIMIT){
        // Generate random key and value
        Data data_struct(rand()%1000,rand()%10);
        // Insert Data struct into B-Tree
        database.insert(data_struct);
        i++;
    }
    i=0;
    //Loop using 10 random generator values to search for in database
    while (i<TEST_KEY_LIMIT){
        // Data struct with random key to be compared with the database keys
        Data comp_data(10,rand()%100);
        BTreeNode <Data> *temp;
        if ( (temp=database.search(comp_data)) ){
            //Call findKey to get a vector of all the indexes where the comp_data key occurs
            std::vector<int> index_array= temp->findKey(comp_data);
            for (int idx:index_array){
                std::cout<<"This key was found: "<<temp->keys[idx]<<std::endl;
            }
       }
       i++;
   }

	return 0;
}
