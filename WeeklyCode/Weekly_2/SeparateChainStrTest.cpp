/**********************************************
* File: SeparateChainingTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "SeparateChaining.h"

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
********************************************/
int main(int argc, char** argv)
{
	// Initialize a Hash Table with a Size of 8
    HashTable<std::string> hTable(8);

	hTable.insert("your"); 
	hTable.insert("enlightenment"); 
	hTable.insert("morrison"); 
	hTable.insert("structures"); 
	hTable.insert("pizza"); 
	hTable.insert("rumpelstiltskin");
	
	hTable.printHash(std::cout);
	
	std::cout << "------" << std::endl;
	std::cout << "Table after removing enlightment" << std::endl;
	hTable.remove("enlightenment");
	
	hTable.printHash(std::cout);


    return 0;
}
