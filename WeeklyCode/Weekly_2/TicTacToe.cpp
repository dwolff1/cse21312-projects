/**********************************************
* File: SeparateChainingTest.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "SeparateChaining.h"

struct TicTacToe {
    char piece;
    int x;
    int y;
    //default constructor
    TicTacToe():piece(' '),x(0),y(0){};
    
    // Data constructor
    TicTacToe(char piece, int x, int y ):piece(piece),x(x), y(y){};
};
    //Less than operator
    bool operator<(const TicTacToe& rhs) const{
        if (key<rhs.key)
            return true;
        return false;
    }
    //More than operator
    bool operator>(const TicTacToe& rhs) const{
        if (key>rhs.key)
            return true;
        return false;
    }
    //Equal to operator
    bool operator==(const TicTacToe& rhs) const{
        if (key!=rhs.key )
            return false;
        return true;
    }
    friend std::ostream& operator<<(std::ostream& outStream, const Data& printData){
        outStream<<" "<<printData.key<<" with data value "<<printData.data;
        return outStream;
    }
    
} ;

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*  
********************************************/
int main(int argc, char** argv)
{
	// Initialize a Hash Table with a Size of 8
    HashTable<int> hTable(8);
	// Row 1
	hTable.insert('X'); 
	hTable.insert('O'); 
	hTable.insert('X'; 
	// Row 2
	hTable.insert('O'); 
	hTable.insert('X');
	hTable.insert(' ');
	// Row 3
	hTable.insert(' ');
	hTable.insert('O');
	hTable.insert('X');
	
	hTable.printHash(std::cout);
	
	std::cout << "------" << std::endl;
	std::cout << "Table after removing 13" << std::endl;
	hTable.remove(13);
	
	hTable.printHash(std::cout);
	
	std::cout << "------" << std::endl;
	std::cout << "Inserting 50 Random Numbers between 0 and 50...";
	// Seed the RNG once, at the start of the program
	srand( time( NULL ) );
	
	for(int ins = 0; ins < 50; ins++){
		hTable.insert(rand() % 50);
	}
	
	std::cout << "Table after insertion: " << std::endl;
	hTable.printHash(std::cout);

    return 0;
}
