#include <iostream>
#include "Expiration.h"

using namespace std;

void checkDate(ostream&, Expiration, Expiration);

int main() {
 
  //ostream os;

  Expiration initialDate(2019, 1, 15);
  
  Expiration Before, sameDay, After;
  Before.setYear(2019);
  Before.setMonth(1);
  Before.setDay(8);

  sameDay.setYear(2019);
  sameDay.setMonth(1);
  sameDay.setDay(15);
  
  After.setYear(2019);
  After.setMonth(1);
  After.setDay(27);

  checkDate(cout, initialDate, Before);
  checkDate(cout, initialDate, sameDay);
  checkDate(cout, initialDate, After);
  
  return 0;

}

void checkDate(ostream& stream, Expiration initialDate, Expiration checkDate) {
  bool expired;
  expired = initialDate.compareDate(checkDate);  
  if (!expired) 
    stream << "The check date " << checkDate << " is before the initial date " << initialDate << endl;
  else 
    stream << "The check date " << checkDate << " is the same day or after the initial date " << initialDate << endl;
}
