#include <iostream>
#include <string>

using namespace std;

class Expiration {

  friend ostream& operator<< (ostream &, Expiration &);

  public:
	
    //constructors
    Expiration();
	  Expiration(const Expiration &v);
	  Expiration(int y, int m, int d);

    //getters
    int getYear();
    string getMonth();
    int getDay();

    //setters
    void setYear(int y);
    void setMonth(int m);
    void setDay(int d);

    //comparison method
    bool compareDate(const Expiration &v);
    
    //overloaded operator
    friend ostream& operator<< (std::ostream &os, Expiration &v) {
        os << v.y << " " << v.getMonth()  <<" " << v.d;
        return os;
    }

  private:
	  string months[12] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
    int y;
    int d;
    int m;
};

