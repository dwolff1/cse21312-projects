#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

#include "Expiration.h"

using namespace std;

Expiration::Expiration() {
  y = 0;
  m = 0;
  d = 0;
}

Expiration::Expiration(const Expiration &v) {
	y = v.y;
	m = v.m;
	d = v.d;
}

Expiration::Expiration(int y, int m, int d) {
	
	this->y = y;
	this->m = m;
	this->d = d;

}

// Getters

int Expiration::getYear() {
  return y;
}

string Expiration::getMonth() {
  string date;
  date = months[this->m - 1];
  return date;
}

int Expiration::getDay() {
  return d;
}

// Setters

void Expiration::setYear(int y) {
  this->y = y;
}

void Expiration::setMonth(int m) {
  this->m = m;
}

void Expiration::setDay(int d) {
  this->d = d;
}

// Compare function

bool Expiration::compareDate(const Expiration &current_date) {
  // this boolean answers the question "is it expired?"
  // so, a return value of "true" indicates "expired"
  
  if (y > current_date.y) {
    return false;
  } else if (y < current_date.y) {
    return true;
  } else if (m > current_date.m) {
    return false;
  } else if (m < current_date.m) {
    return true;
  } else if (d > current_date.d) {
    return false;
  } else if (d < current_date.m) {
    return true;
  }
  else{
      return true;
  }
}




